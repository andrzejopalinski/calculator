import java.lang.constant.Constable;

public class Calculator {
    public static double add(int a, int b){
        return (double)a + (double)b;
    }
    public static double subtract(int a, int b){
        return (double)a - (double)b;
    }
    public static double multiply(int a, int b){
        return (double)a * (double)b;
    }
    public static Number divide(int a, int b){
        if(b==0){
            System.out.println("Sam chciałbym wiedzieć jak dzielić przez 0 :)");
            return null;
        }else
            return((double)a / (double)b);
    }
    public static double power (int a, int b){
        return Math.pow(a,b);
    }
}

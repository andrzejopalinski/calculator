import java.util.Scanner;

public class CalculatorLauncher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        String operator = scanner.next();
        int b = scanner.nextInt();

        if (operator.equals("+")) {
            System.out.println(Calculator.add(a, b));
        } else if (operator.equals("-")) {
            System.out.println(Calculator.subtract(a, b));
        } else if (operator.equals("*")) {
            System.out.println(Calculator.multiply(a, b));
        } else if (operator.equals("/")) {
            System.out.println(Calculator.divide(a, b));
        } else if (operator.equals("^")) {
            System.out.println(Calculator.power(a, b));
        }
    }
}
